#!/usr/bin/python3

import sys
import json
import codecs
import urllib.error
import urllib.parse
import urllib.request
from socket import timeout

import threading
import queue

from token_cache import TOKEN

from typing import List


# curl -X GET "https://public-api.tracker.gg/v2/rocket-league/standard/profile/<platform>/<profile>" -H "TRN-Api-Key: <key>"


red = "\033[91m"
grn = "\033[92m"
blu = "\033[94m"
end = "\033[00m"

rank_colors = {
    "unr": "\033[38;5;238m",
    "brz": "\033[38;5;094m",
    "slv": "\033[38;5;243m",
    "gld": "\033[38;5;172m",
    "plt": "\033[38;5;073m",
    "dmd": "\033[38;5;026m",
    "cmp": "\033[38;5;135m",
    "grc": "\033[38;5;088m",
    "ssl": "\033[38;5;165m",
}


profile_queue = queue.Queue()


def storeInQueue(f):
    def wrapper(*args):
        profile_queue.put(f(*args))

    return wrapper


def main():
    profiles_from_args = get_profiles_from_args()

    threads = [
        threading.Thread(target=make_request, args=(prof["Username"], prof["Platform"]))
        for prof in profiles_from_args
    ]
    for thread in threads:
        thread.start()

    profiles = {}
    for prof in profiles_from_args:
        player = profile_queue.get()  # returns stat dict or None
        name = list(player.keys())[0]  # FIXME
        profiles[name] = player[name]

    stats = []
    for prof in profiles_from_args:
        profile = profiles[prof["Username"]]
        if not profile:
            # handle the case where the url can't be loaded, etc.
            continue

        stat_dict = {}

        stat_dict["Name"] = profile["data"]["platformInfo"]["platformUserHandle"]
        stat_dict["Platform"] = prof["Platform"]
        # TODO: truncate longer names, maybe to ~11 characters including ellipsis
        stat_dict["maxstrlen"] = max(
            max(len(stat_dict["Name"]), len(stat_dict["Platform"])), 8
        )
        stat_dict["Ranks"] = {}
        playlists = profile["data"]["segments"]
        for playlist in playlists:
            if playlist["type"] == "playlist":
                playlist_fullname = playlist["metadata"]["name"]
                playlist_nickname = playlist_dict[playlist_fullname]
                stat_dict["Ranks"][playlist_nickname] = {
                    "MMR": playlist["stats"]["rating"]["value"],
                    "Rank": playlist["stats"]["tier"]["metadata"]["name"],
                    "Color": rank_name_to_color(playlist["stats"]["tier"]["metadata"]["name"]),
                    "Streak": (
                        int(playlist["stats"]["winStreak"]["displayValue"])
                        if playlist["stats"]["winStreak"]["displayValue"]
                        else 0
                    ),
                }

        stats.append(stat_dict)

    # Print it all pretty
    string_platform = "Plat:    "
    string_username = "User:    "
    string_3v3 = "3v3:     "
    string_2v2 = "2v2:     "
    string_solo3s = "Solo3s:  "
    string_1v1 = "1v1:     "
    string_rumble = "Rumble:  "
    string_hoops = "Hoops:   "
    string_dropsh = "Dropsh:  "
    string_snowday = "Snow:    "
    string_tournies = "Tourni:  "
    string_unranked = "Unrank:  "
    string_spacer = "-------  "

    for player in stats:
        string_platform += "{:>{msl}}  ".format(player["Platform"], msl=player["maxstrlen"])
        string_username += "{:>{msl}}  ".format(player["Name"], msl=player["maxstrlen"])
        string_3v3 += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("3v3", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("3v3", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("3v3", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_2v2 += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("2v2", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("2v2", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("2v2", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_solo3s += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("Solo3s", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("Solo3s", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("Solo3s", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_1v1 += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("1v1", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("1v1", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("1v1", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_rumble += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("Rumble", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("Rumble", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("Rumble", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_hoops += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("Hoops", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("Hoops", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("Hoops", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_dropsh += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("Dropsh", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("Dropsh", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("Dropsh", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_snowday += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("Snow", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("Snow", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("Snow", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_tournies += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("Tournie", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("Tournie", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("Tournie", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_unranked += "{}{:>{msl}}  ".format(
            rank_colors[player["Ranks"].get("Unrank", {"Color": "unr"})["Color"]],
            str(player["Ranks"].get("Unrank", {"MMR": "----"})["MMR"])
            + end
            + streak_format(player["Ranks"].get("Unrank", {"Streak": 0})["Streak"]),
            msl=player["maxstrlen"] + (3 * len(end)),
        )
        string_spacer += "-" * (player["maxstrlen"]) + "  "

    print(string_platform)
    print(string_username)
    print(string_spacer)
    print(string_3v3)
    print(string_2v2)
    # print(string_solo3s)
    print(string_1v1)
    print(string_spacer)
    print(string_rumble)
    print(string_hoops)
    print(string_dropsh)
    print(string_snowday)
    print(string_spacer)
    print(string_tournies)
    print(string_unranked)


def get_profiles_from_args() -> List:
    prof_args = sys.argv[1:]
    profiles = []  # ["username", "platform"]
    for prof in prof_args:
        if prof[0:2] == "s:":
            profiles.append({"Username": prof[2:], "Platform": "steam"})
        elif prof[0:2] == "x:":
            profiles.append({"Username": prof[2:], "Platform": "xbl"})
        elif prof[0:2] == "p:":
            profiles.append({"Username": prof[2:], "Platform": "psn"})
        elif prof[0:2] == "e:":
            profiles.append({"Username": prof[2:], "Platform": "epic"})
        else:
            profiles.append({"Username": prof, "Platform": "steam"})

    return profiles


@storeInQueue
def make_request(username: str, platform: str):
    print('Requesting "{}" on {}'.format(username, platform))
    url = "https://public-api.tracker.gg/v2/rocket-league/standard/profile/{}/{}".format(
        platform, username
    )

    req = urllib.request.Request(url, method="GET")
    req.add_header("TRN-Api-Key", TOKEN)
    req.add_header("User-Agent", "Mozilla/5.0")
    try:
        res = urllib.request.urlopen(req)
    except urllib.error.HTTPError:
        print("Could not fetch {} due to TRN error.".format(username))
        return {username: None}
    except timeout:
        print("Timed out while fetching {}.".format(username))
        return {username: None}
    except Exception as e:
        print(e)
        print("Could not fetch {} due to other error.".format(username))
        return {username: None}

    reader = codecs.getreader("utf-8")

    return {username: json.load(reader(res))}


playlist_dict = {
    "Un-Ranked": "Unrank",
    "Ranked Duel 1v1": "1v1",
    "Ranked Doubles 2v2": "2v2",
    "Ranked Solo Standard 3v3": "Solo3s",
    "Ranked Standard 3v3": "3v3",
    "Hoops": "Hoops",
    "Rumble": "Rumble",
    "Dropshot": "Dropsh",
    "Snowday": "Snow",
    "Tournament Matches": "Tournie",
}


def streak_format(value: int) -> str:
    # Takes integer for streak and colors the output
    if value == 0:
        return "(" + end + str(0) + end + ")"
    elif value > 0:
        return "(" + grn + str(value) + end + ")"
    elif value < 0:
        return "(" + red + str(-1 * value) + end + ")"
    else:
        return str(0)


def rank_name_to_color(colorname: str) -> str:
    if "bronze" in colorname.lower():
        return "brz"
    elif "silver" in colorname.lower():
        return "slv"
    elif "gold" in colorname.lower():
        return "gld"
    elif "platinum" in colorname.lower():
        return "plt"
    elif "diamond" in colorname.lower():
        return "dmd"
    elif "grand" in colorname.lower():
        return "grc"
    elif "champion" in colorname.lower():
        return "cmp"
    elif "legend" in colorname.lower():
        return "ssl"
    else:
        return "unr"


if __name__ == "__main__":
    main()
